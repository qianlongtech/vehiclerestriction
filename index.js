/*global $*/
$(document).ready(function () {
    'use strict';
    var init, bind;
    bind = function () {
        $('.page1 .map li img').hover(function (event) {
            var here = $(event.target),
                index = $('.page1 .map li img').index(here),
                there = $('.page1 .map li p').eq(index);
            there.css({
                left: here.position().left + 80 + 'px',
                top: here.position().top - 10 + 'px'
            });
        }, function () {});

        $('.page2 .chart2 .btngroup div').hover(function (event) {
            var here = $(event.target),
                index = $('.page2 .btngroup div').index(here),
                there = $('.page2 .chart2 li');
            here.siblings().removeClass('active');
            here.addClass('active');
            if (index) {
                $('.g').addClass('hide');
                $('.number span').html('3');
            } else {
                $('.g').removeClass('hide');
                $('.number span').html('6');
            }
        }, function () {});

        $('.page3 li').hover(function (event) {
            var here = $(event.target),
                index = $('.page3 li').index(here),
                i = (index > -1) ? index : 0;
            $('.page3 .circle').animate({
                top: 36 + 96 * i + 'px'
            }, 100, function () {
                here.siblings().removeClass('active');
                here.addClass('active');
            });
        }, function () {});

        $('.page4 .btngroup div').hover(function (event) {
            var here = $(event.target),
                index = $('.page4 .btngroup div').index(here),
                there = $('.page4 .tablegroup img').eq(index);
            here.siblings().removeClass('active');
            here.addClass('active');
            there.siblings().removeClass('active');
            there.addClass('active');
        }, function () {});
    };
    init = (function () {
        var tick = 0;
        $('#container').fadeIn();
        $('#container').fullpage({
            navigation: true
        });
        setInterval(function () {
            $('.page0').css({
                'background-position': tick + '% bottom'
            });
            if (tick < -1000) {
                tick = 0;
            } else {
                tick -= 0.3;
            }
        });
        bind();
    }());
});
